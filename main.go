package main

import (
	"github.com/robfig/cron/v3"
	"google-calendar-chat/google"
	"log"
	"time"
)

func main() {
	c := cron.New()
	c.AddFunc("0 4 * * *", cronF)
	c.Run()
}

func cronF() {
	client, err := google.Client()
	if err != nil {
		log.Fatalln(err)
	}
	api := google.Api{
		Calendar: google.NewCalendar(client),
		Chat:     google.NewChat(client),
	}
	for i := 1; i <= 3; i++ {
		err = api.GetAndSend()
		if err == nil {
			log.Println("Job done.")
			break
		}
		log.Println(err)
		time.Sleep(time.Second * 10)
	}
}