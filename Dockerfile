FROM golang:1.13.5-alpine3.11
WORKDIR /go/src/app
COPY . .
RUN go mod download
RUN go build -o script .
RUN apk add nano
CMD ["./script"]
