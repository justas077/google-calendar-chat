package google

import (
	"encoding/json"
	"fmt"
	"google.golang.org/api/calendar/v3"
	"google.golang.org/api/chat/v1"
	"io/ioutil"
	"log"
	"net/http"
)

const chatFile = "chat.json"

func NewChat(client *http.Client) *chat.Service {
	srv, err := chat.New(client)
	if err != nil {
		log.Fatalf("Unable to retrieve chat client: %v", err)
	}
	return srv
}

func (api *Api) createMessage(events []*calendar.Event) string {
	message := ""
	for _, e := range events {
		if e.Creator != nil {
			message += fmt.Sprintf("`%s` %s", e.Creator.Email, e.Summary)
			if e.Description != "" {
				message += " - _" + e.Description + "_"
			}
			message += "\n"
		}
	}
	return message
}

func (api *Api) sendMessage(message string) error {
	room, err := api.getRoom()
	if err != nil {
		return err
	}
	_, err = api.Chat.Spaces.Messages.Create(room, &chat.Message{Text: message}).Do()
	return err
}

func (api *Api) getRoom() (string, error) {
	b, err := ioutil.ReadFile(chatFile)
	if err != nil {
		return "", err
	}
	var ch map[string]string
	_ = json.Unmarshal(b, &ch)

	pp, _ := api.Chat.Spaces.List().Fields().Do()
	for _, p := range pp.Spaces {
		if p.DisplayName == ch["room"] && p.DisplayName != "" {
			return p.Name, nil
		}
	}
	return "", nil
}
