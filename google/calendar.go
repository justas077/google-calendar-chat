package google

import (
	"encoding/json"
	"fmt"
	"google.golang.org/api/calendar/v3"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

const listFile = "calendar_list.json"

func NewCalendar(client *http.Client) *calendar.Service {
	srv, err := calendar.New(client)
	if err != nil {
		log.Fatalf("Unable to retrieve Calendar client: %v", err)
	}
	return srv
}

func (api *Api) getEvents(tMin, tMax time.Time) ([]*calendar.Event, error) {
	evList := make([]*calendar.Event, 0)
	ids, err := calendarIds()
	if err != nil {
		return evList, err
	}
	for _, c := range ids {
		events, err := api.Calendar.Events.List(c).ShowDeleted(false).
			SingleEvents(true).
			TimeMin(tMin.Format(time.RFC3339)).
			TimeMax(tMax.Format(time.RFC3339)).
			OrderBy("startTime").Do()
		if err != nil {
			log.Println(fmt.Sprintf("Unable to retrieve events from %s: %v", c, err))
			continue
		}
		for _, item := range events.Items {
			evList = append(evList, item)
		}
	}
	return evList, nil
}

func calendarIds() (ids []string, err error) {
	ids = make([]string, 0)
	cal, err := ioutil.ReadFile(listFile)
	if err != nil {
		return
	}
	_ = json.Unmarshal(cal, &ids)
	return
}
