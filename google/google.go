package google

import (
	"context"
	"encoding/json"
	"fmt"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/calendar/v3"
	"google.golang.org/api/chat/v1"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

const serviceAccFile = "credentials/service_account.json"
const tokenFile = "credentials/token.json"

type Api struct {
	Calendar *calendar.Service
	Chat     *chat.Service
}

func (api *Api) GetAndSend() error {
	events, err := api.getEvents(time.Now(), time.Now().Add(time.Hour*24))
	if err != nil {
		return err
	}
	if len(events) == 0 {
		log.Println("No events was found.")
		return nil
	}
	message := api.createMessage(events)
	err = api.sendMessage(message)
	return err
}

func Client() (*http.Client, error) {
	b, err := ioutil.ReadFile(serviceAccFile)
	if err != nil {
		return nil, err
	}
	token, err := tokenFromFile(tokenFile)
	if err != nil || !token.Valid() {
		cred, err := google.CredentialsFromJSON(context.Background(), b, calendar.CalendarReadonlyScope, "https://www.googleapis.com/auth/chat.bot")
		if err != nil {
			return nil, err
		}
		tok, err := cred.TokenSource.Token()
		if err != nil {
			return nil, err
		}
		saveToken(tokenFile, tok)
		token = tok
	}
	c := oauth2.Config{}
	return c.Client(context.Background(), token), nil
}

func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}
